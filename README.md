# Symfony Docker Example

This is an example for a **Symfony** application that uses **Docker**.

Any suggestions are welcomed.

## Instructions
1. Map in **/etc/hosts** file, **mysql** and **your-vhost.local**, default **symfony-docker.local**, to go to **localhost**.
2. Run `composer install` in project root.
3. Run `cd docker/ && docker-composer build && docker-composer up -d` in project root.
4. Go to **https://symfony-docker.local/** in the browser of your choice.

The security warning you will receive in the browser it is because the certificates are self signed.

**All the magic is in the docker folder, it can be moved to any other application**.

**You may clone and modify it as you wish**.
